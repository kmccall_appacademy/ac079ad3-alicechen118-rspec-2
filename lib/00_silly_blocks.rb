def reverser(&prc)
  string = prc.call
  words = string.split(" ")
  words = words.map { |word| word.reverse }
  words.join(" ")
end

def adder(surplus=1, &prc)
  surplus + prc.call
end

def repeater(times=1, &prc)
  times.times { prc.call }
end
